# usage:
# python spc.py test.pp
# python spc_runner.py test.spc

import sys
from enum import Enum, unique
import string

print "Usage: python spc.py {hello}.pp"
print "Compiling file ", sys.argv[1]
#print "Starting Scanner"

from spc_tokens import *
from spc_scanner import *
from spc_symbol_table import *
from spc_operation_list import *
        

# File stack:
# files[{'name': "filename.pp", 'row': 5, 'column': 12}, {...}]
files = [{'name': sys.argv[1], 'row': 1, 'column': 1, 'code':'', 'index':0}]

f = open(files[-1]['name'], 'r')
files[-1]['code'] = f.read()

token_list = []

#print "code:", files[-1]['code']
#print

symbols = SymbolTable()
op_list = OperationList()
scanner = Scanner(files[-1]['code'], files, token_list)

while(True):
    try:
        new_token = scanner.NextToken()
        token_list.append(new_token)
        
        if new_token.GetType() == "eof":
            break
        
        """print new_token
        print "*"+repr(scanner.read)+"*"
        print "row =", files[-1]['row']
        print "column =", files[-1]['column']
        print""" 
        
    except EOFError:
        print scanner.TokenFromRead()
        print "We're done here"
        break

#print token_list

token_iter = iter(enumerate(token_list))
def EnsureTokenTypes(in_tokens, token_types):
    for i, token in enumerate(in_tokens):
        if token.type != token_types[i]:
            print "Error: '"+ str(token_types[i])\
                +"' expected, but found", token.name
        else:
            pass
    

to_table = []
check_next_semi = False

for i, token in enumerate(token_list):
    
    if token.type == TokenType.id and\
        token_list[i+1].type == TokenType.op_open_bracket:
            #EnsureTokenTypes(token_list[i+2:i+4], 
            #    [TokenType.int_lit, TokenType.op_close_bracket])
            
            new_arg = []
            j = i + 2
            temp_token = token_list[j]
            
            while temp_token.type != TokenType.op_close_bracket:
                new_arg += [temp_token]
                j += 1
                temp_token = token_list[j]
            
            token.type = TokenType.array_el
            id_name = token.name
            
            new_func = Operation(token, new_arg)
            new_func.EvalArgs()
            
            token.value.update({"index": token_list[i+2], "id": id_name, 
                                "arg": new_func.args})
            
            del token_list[i+1:j+1]
            
for i, token in token_iter:
    
    if token.type == TokenType.op_semi:
        if check_next_semi:
            check_next_semi = False
        
        
    
    if token.type == TokenType.kw_var:
        to_table = []
        while True:
            temp_id = next(token_iter)[1]
            i += 1
            
            if temp_id.type != TokenType.id:
                print "Error at Line - "+temp_id.column\
                        +": identifier must follow var"
            
            to_table += [temp_id]
            
            if token_list[i+1].type == TokenType.op_comma:
                next(token_iter)
                i += 1
                
            else:
                break
        
        next(token_iter)
        i, token = next(token_iter)
        temp_type = token
        
        if temp_type.type == TokenType.kw_array:
            EnsureTokenTypes(token_list[i+1:i+6], 
                             [TokenType.op_open_bracket, TokenType.int_lit, 
                                TokenType.op_2dots, TokenType.int_lit, 
                                TokenType.op_close_bracket, TokenType.kw_of])
            
            temp_type.value.update({"min": token_list[i+2],
                                    "max": token_list[i+4],
                                    "type": token_list[i+7]})
            
            for i in range(0, 7):
                next(token_iter)
            
        for symb in to_table:
            symb.value = {'type': temp_type}
            symbols.Add(symb)
        
        i, token = next(token_iter)
        EnsureTokenTypes([token], [TokenType.op_semi])
    
    elif token.type == TokenType.kw_procedure:
        EnsureTokenTypes([token_list[i+1]], [TokenType.id])
        
        temp_func_id = next(token_iter)[1]
        
        EnsureTokenTypes([ next(token_iter)[1] ], 
            [ TokenType.op_semi ])
        
        """i, temp_arg = next(token_iter)
        temp_comma = token_list[i+1]
        temp_arg_list = []
        while temp_comma.type == TokenType.op_comma:
            temp_arg_list += [temp_arg]
            temp_comma = next(token_iter)[1]
            i, temp_arg = next(token_iter)
        
        for arg in temp_arg_list:
            arg.value.update({"type": temp_arg})
        
        
        temp_func = Token(TokenType.func, {'args': temp_arg_list,
                                               'token': temp_func_id},
                            name = temp_func_id.name)"""
        temp_func = Token(TokenType.func, {'args': [], 'token': temp_func_id},
                            name = temp_func_id.name)
        symbols.Add(temp_func)
    
    elif token.type == TokenType.op_colon_eq:
        target = []
        t_iter = i-1
        not_semi = token_list[t_iter]
        if not_semi.type in [TokenType.op_semi, TokenType.kw_begin,
                                TokenType.kw_program, TokenType.kw_repeat,
                                TokenType.kw_until, TokenType.kw_do,
                                TokenType.kw_for, TokenType.kw_while,
                                TokenType.kw_else, TokenType.kw_if, 
                                TokenType.location]:
            print "no left hand in assignment at line:"
            print token.row
            
        while not_semi.type not in [TokenType.op_semi, TokenType.kw_begin,
                                TokenType.kw_program, TokenType.kw_repeat,
                                TokenType.kw_until, TokenType.kw_do,
                                TokenType.kw_for, TokenType.kw_while,
                                TokenType.kw_else, TokenType.kw_if, 
                                TokenType.location]:
            try:
                target += [token_list[t_iter]]
            except: 
                break
            
            t_iter -= 1
            not_semi = token_list[t_iter]
        
        source = []
        s_iter = i+1
        not_semi = token_list[s_iter]
        if not_semi.type in [TokenType.op_semi, TokenType.kw_begin,
                                                TokenType.kw_program]:
            print "no right hand in assignment at line:"
            print token.row
            
        while not_semi.type not in [TokenType.op_semi, TokenType.kw_begin,
                                                TokenType.kw_program]:
            try:
                source += [token_list[s_iter]]
            except: 
                break
            
            s_iter += 1
            not_semi = token_list[s_iter]
        
        
        temp_func = Operation(token, [target, source])
        temp_func.EvalArgs()
        
        op_list.Add(temp_func)
        
    elif token.type == TokenType.id and\
        (token.name == "write" or token.name == "writeln"):
        EnsureTokenTypes([next(token_iter)[1]], [TokenType.op_open_paren])
        
        printable_tokens = [TokenType.string_lit, TokenType.id, TokenType.real_lit,
                            TokenType.int_lit, TokenType.op_plus, TokenType.op_minus,
                            TokenType.kw_not, TokenType.array_el]
        
        i, temp_var = next(token_iter)
        if temp_var.type not in printable_tokens:
            print "Error: at line", temp_var.row, "write(String) expected",\
            "but write(", temp_var, ") found"
            
        temp_args = []
        
        
        while temp_var.type != TokenType.op_semi:
            if token_list[i+1].type == TokenType.op_comma or\
                token_list[i+1].type == TokenType.op_close_paren:
                temp_args.append(temp_var)
                next(token_iter)
                i, temp_var = next(token_iter)
            else:
                temp_arg = []
                while True:
                    temp_arg.append(temp_var)
                    i, temp_var = next(token_iter)
                    if temp_var.type == TokenType.op_comma:
                        i, temp_var = next(token_iter)
                        temp_args.append(temp_arg)
                        break
                    elif token_list[i + 1].type == TokenType.op_semi:
                        i, temp_var = next(token_iter)
                        temp_args.append(temp_arg)
                        break
        
        
        temp_func = Operation(token, temp_args)
        temp_func.EvalArgs()
        op_list.Add(temp_func)
    
    elif token.type == TokenType.kw_if:
        cond = []
        j = i
        while True:
            j += 1
            temp_token = token_list[j]
            
            then_id = 0
            then_location = 0
            
            if temp_token.type == TokenType.kw_then:
                then_id = temp_token.id
                then_location = j
            
                token.value.update({"then_id": then_id,
                    "then_loc": 0, "cond": cond})
                
                token_list[j].value.update({"if_id": token.id})
                
                break
            
            else:
                cond += [temp_token]
            
        k = j
        k += 1
        temp_token = token_list[k]
        jmp_id = 0
        
        if temp_token.type == TokenType.kw_begin:
            token.value.update({"begin_exists": True,
                "begin_id": temp_token.id,
                "begin_loc": 0})
            token_list[k].value.update({"if_exists": True,
                "if_id": token.id})
                
            while True:
                k += 1
                try:
                    temp_token = token_list[k]
                except:
                    print "Error: if statement not finished"
                    quit()
                
                if temp_token.type == TokenType.kw_end:
                    token.value.update({"end_exists": True,
                        "end_id": temp_token.id, "jmp_id": temp_token.id,
                        "end_loc": 0})
                    token_list[k].value.update( {"if_exists": True,
                        "if_id": token.id, "is_loc": True})
                    
                    jmp_id = temp_token.id
                    
                    break
        else:
            while True:
                if temp_token.type == TokenType.op_semi:
                    token_list[k].value.update({"is_loc": True,
                        "if_exists": True, "if_id": token.id})
                    
                    token.value.update({"end_exists": False, 
                        "semi_exists": True, "semi_id": temp_token.id,
                        "semi_loc": 0, "jmp_id": temp_token.id})
                    
                    jmp_id = temp_token.id
                    
                    check_next_semi = True
                    
                    new_token = temp_token.MakeCopy()
                    new_token.type = TokenType.location
                    new_token.name = "location"
                    new_token.value.update({'is_loc': True})
                    token_list.insert(k+1, new_token)
                    
                    break
                
                k += 1
                try:
                    temp_token = token_list[k]
                except:
                    print "Error: if statement not finished"
                    quit()
                
        temp_func = Operation(token, [cond, jmp_id])
        temp_func.EvalArgs()
        op_list.Add(temp_func)
        
    elif token.type == TokenType.kw_while:
        cond = []
        j = i
        while True:
            j += 1
            temp_token = token_list[j]
            
            do_id = 0
            do_location = 0
            
            if temp_token.type == TokenType.kw_do:
                do_id = temp_token.id
                do_location = j
            
                token.value.update({"do_id": do_id,
                    "do_loc": 0, "cond": cond})
                
                token_list[j].value.update({"while_id": token.id})
                
                break
            
            else:
                cond += [temp_token]
            
        k = j
        k += 1
        temp_token = token_list[k]
        jmp_id = 0
        
        jmp_token = temp_token.MakeCopy()
        jmp_token.type = TokenType.jmp
        jmp_token.name = "jmp"
        jmp_token.value.update({'target': token.id})
        
        if temp_token.type == TokenType.kw_begin:
            token.value.update({"begin_exists": True,
                "begin_id": temp_token.id,
                "begin_loc": 0})
            token_list[k].value.update({"while_exists": True,
                "while_id": token.id})
                
            while True:
                k += 1
                try:
                    temp_token = token_list[k]
                except:
                    print "Error: while statement not finished"
                    quit()
                
                if temp_token.type == TokenType.kw_end:
                    # while
                    token.value.update({"end_exists": True,
                        "end_id": temp_token.id, "jmp_id": temp_token.id,
                        "end_loc": 0})
                    
                    
                    # end
                    token_list[k].value.update( {"while_exists": True,
                        "while_id": token.id, "is_loc": True,
                        "insert_jmp": jmp_token})
                    
                    jmp_id = temp_token.id
                    
                    break
        else:
            while True:
                if temp_token.type == TokenType.op_semi:
                    # semi
                    token_list[k].value.update({"is_loc": True,
                        "while_exists": True, "while_id": token.id, 
                        "insert_jmp": jmp_token})
                    
                    # while
                    token.value.update({"end_exists": False, 
                        "semi_exists": True, "semi_id": temp_token.id,
                        "semi_loc": 0, "jmp_id": temp_token.id})
                    
                    #token_list.insert(k+1, jmp_token)
                    
                    jmp_id = temp_token.id
                    
                    check_next_semi = True
                    
                    new_token = temp_token.MakeCopy()
                    new_token.type = TokenType.location
                    new_token.name = "location"
                    new_token.value.update({'is_loc': True})
                    token_list.insert(k+1, new_token)
                    
                    break
                
                k += 1
                try:
                    temp_token = token_list[k]
                except:
                    print "Error: while statement not finished"
                    quit()
        
        loc_token = token.MakeCopy(new_id = False)
        loc_token.type = TokenType.location
        loc_token.name = "location"
        temp_func = Operation(loc_token, [token.id])
        op_list.Add(temp_func)
        
        temp_func = Operation(token, [cond, jmp_id])
        temp_func.EvalArgs()
        op_list.Add(temp_func)
        
    elif token.type == TokenType.kw_repeat:
        loc_token = token.MakeCopy(new_id = False)
        loc_token.type = TokenType.location
        loc_token.name = "location"
        temp_func = Operation(loc_token, [token.id])
        op_list.Add(temp_func)
        
        j = i
        while True:
            j += 1
            temp_token = token_list[j]
            
            if temp_token.type == TokenType.kw_until:
                # repeat
                token.value.update({"until_id": token.id})
                
                # until
                token_list[j].value.update({"jmp_id": token.id})
                until_token = token_list[j]
                jmp_id = token.id
                
                break
            
    elif token.type == TokenType.kw_until:
        jmp_id = token.value['jmp_id']
        k = j
        k += 1
        temp_token = token_list[k]
        
        cond = []
        while temp_token.type != TokenType.op_semi:
            cond += [temp_token]
            k += 1
            try:
                temp_token = token_list[k]
            except:
                print "Error: repeat-until loop not completed"
        
        
        temp_func = Operation(until_token, [cond, jmp_id])
        temp_func.EvalArgs()
        op_list.Add(temp_func)
        
    elif (token.type == TokenType.kw_end and\
        token.value.get("is_loc", False)) or\
        token.type == TokenType.location:
            
            if token.value.get("insert_jmp", False):
                jmp_token = token.value['insert_jmp']
                temp_func = Operation(jmp_token, [jmp_token.value['target']])
                op_list.Add(temp_func)
                
            loc_token = token.MakeCopy()
            loc_token.type = TokenType.location
            loc_token.name = "location"
            temp_func = Operation(loc_token, [token.id])
            op_list.Add(temp_func)
                
        
    elif token.type == TokenType.jmp:
        temp_func = Operation(token, [token.value['target']])
        op_list.Add(temp_func)
    
    elif token.type == TokenType.id:
        symbols.FindById(token.name)
            
        
        
        
#print op_list
#print symbols
#print "token list is", token_list
#print "symbols is as follows"
#print symbols.CreateStackCode()
#print "\nops is as follows"
#print op_list.CreateStackCode()

stack_code = symbols.CreateStackCode()
stack_code += op_list.CreateStackCode()

stack_filename = files[0]['name'].split('.')[-2]
stack_filename += ".spc"

stack_file = open(stack_filename, 'w')
stack_file.write(stack_code)








