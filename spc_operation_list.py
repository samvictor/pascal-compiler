from spc_tokens import *

# very low, low, medium, high, very high
precedence_order = [[TokenType.kw_or],
                    [TokenType.kw_and],
                    [TokenType.op_eq, TokenType.op_not_eq,TokenType.op_less,
                        TokenType.op_greater, TokenType.op_less_eq,
                        TokenType.op_greater_eq],
                    [TokenType.op_minus, TokenType.op_plus], 
                    [TokenType.op_multiply, TokenType.op_div], 
                    [TokenType.kw_not],
                    [TokenType.op_minus, TokenType.op_plus],
                    [TokenType.op_open_paren]]

# Operation List
class Operation:
    op = None
    args = []
    type = TokenType.func
    
    def __init__(self, operation, arguments):
        # operation should be token of type id or op_...
        self.op = operation
        self.args = arguments
        
    def __str__(self):
        return "\n" + self.op.name +" <- "+ str(self.args)
    
    def __repr__(self):
        return self.__str__()
    
    def EvalArgs(self):
        for i, a in enumerate(self.args):
            self.args[i] = self.EvalNthArg(i)
        
    def EvalNthArg(self, arg_num):
        if type(self.args[arg_num]) != list or len( self.args[arg_num] ) < 2:
            return self.args[arg_num]
        
        new_list = []
        current_list = self.args[arg_num]
        
        close_paren_location = 0
        for i, a in enumerate(current_list):
            if a.type == TokenType.op_open_paren:
                for j, e in enumerate(current_list):
                    if j <= i:
                        continue
                    
                    if e.type == TokenType.op_close_paren:
                        break
                    
                    if j == len(current_list) - 1:
                        print "Error: unbalanced parentheses"
                    
                    close_paren_location = j
                
                temp_func = Operation(a, [current_list[i+1:close_paren_location+1]])
                temp_func.EvalArgs()
                new_list = current_list[0:i]
                new_list.append(temp_func)
                new_list += current_list[close_paren_location+2:]
                break
                
        
        if new_list:
            current_list = new_list
            new_list = []
        
        # unaries
        for i, a in enumerate(current_list):
            if a.type in precedence_order[6] and (i == 0 or
                current_list[i-1].type in precedence_order[0] or
                current_list[i-1].type in precedence_order[1] or
                current_list[i-1].type in precedence_order[2] or
                current_list[i-1].type in precedence_order[3] or
                current_list[i-1].type in precedence_order[4] or
                current_list[i-1].type in precedence_order[5] or
                current_list[i-1].type in precedence_order[6] or
                current_list[i-1].type in precedence_order[7] ):
                    temp_func = Operation(a, [current_list[i+1], current_list[:i],
                                                current_list[i+2:]])
                    temp_func.EvalArgs()
                    new_list += temp_func.args[1]
                    new_list.append(temp_func)
                    new_list += temp_func.args[1]
                    temp_func.args = [Token(TokenType.int_lit, 0, "0"),
                                            temp_func.args[0]]
                    break
                
        
        if new_list:
            current_list = new_list
            new_list = []
        
        # unaries
        for i, a in enumerate(current_list):
            if a.type in precedence_order[5]:
                temp_func = Operation(a, [current_list[i+1], current_list[:i],
                                            current_list[i+2:]])
                temp_func.EvalArgs()
                new_list += temp_func.args[1]
                new_list.append(temp_func)
                new_list += temp_func.args[1]
                temp_func.args = [temp_func.args[0]]
                break
                
        
        if new_list:
            current_list = new_list
            new_list = []
        
        for i, a in enumerate(current_list):
            if a.type in precedence_order[0]:
                left_args = current_list[0:i]
                right_args = current_list[i+1: len(current_list) ]
                temp_func = Operation(a, [left_args, right_args])
                temp_func.EvalArgs()
                new_list.append(temp_func)
                break
        
        if new_list:
            current_list = new_list
            new_list = []
        
        for i, a in enumerate(current_list):
            if a.type in precedence_order[1]:
                left_args = current_list[0:i]
                right_args = current_list[i+1: len(current_list) ]
                temp_func = Operation(a, [left_args, right_args])
                temp_func.EvalArgs()
                new_list.append(temp_func)
                break
        
        if new_list:
            current_list = new_list
            new_list = []
        
        for i, a in enumerate(current_list):
            if a.type in precedence_order[2]:
                left_args = current_list[0:i]
                right_args = current_list[i+1: len(current_list) ]
                temp_func = Operation(a, [left_args, right_args])
                temp_func.EvalArgs()
                new_list.append(temp_func)
                break
        
        if new_list:
            current_list = new_list
            new_list = []
        
        for i, a in enumerate(current_list):
            if a.type in precedence_order[3]:
                left_args = current_list[0:i]
                right_args = current_list[i+1: len(current_list) ]
                temp_func = Operation(a, [left_args, right_args])
                temp_func.EvalArgs()
                new_list.append(temp_func)
                break
        
        if new_list:
            current_list = new_list
            new_list = []
        
        for i, a in enumerate(current_list):
            if a.type in precedence_order[4]:
                left_args = current_list[0:i]
                right_args = current_list[i+1: len(current_list) ]
                temp_func = Operation(a, [left_args, right_args])
                temp_func.EvalArgs()
                new_list.append(temp_func)
                break
        
        if new_list:
            current_list = new_list
            new_list = []
        
        return current_list
        

class OperationList:
    op_list = []
    current_op = None
    
    def __init__(self):
        pass
    
    def __str__(self):
        return str(self.op_list)
    
    def Add(self, op):
        self.op_list += [op]
        self.current_op = self.op_list[-1]
    
    def GetCurrent(self):
        return self.current_op
        
    def CreateStackCode(self):
        code = "code:\n"
        
        for op in self.op_list:
            for j, arg in reversed(list(enumerate(op.args))):
                if type(arg) == list:
                    code += self.StackCodeFromOp(arg)
                elif arg.__class__.__name__ == "Operation":
                    code += arg.type.name
                    code += "<->"
                    code += arg.name
                    code += "\n"
                else:
                    code += self.StackCodeFromOp(arg)
            
            code += "func<->"
            code += op.op.name
            
            if op.op.type == TokenType.location:
                code += "<->"
                code += str(op.args[0])
            
            code += "\n"
            code += "message<->"
            code += "done\n"
            
        return code + ";"
    
    def StackCodeFromOp(self, arg):
        code = ""
        if type(arg) == list:
            for a in arg:
                code += self.StackCodeFromOp(a)
        
            return code
        
        elif arg.__class__.__name__ == "Operation":
            for a in arg.args:
                code += self.StackCodeFromOp(a)
            
            code += "func<->"
            code += arg.op.name
            code += "\n"
            return code
        
        else:
            if arg.__class__.__name__ == "Token":
                code += str(arg.type.name)
                code += "<->"
                code += arg.name
                if arg.type.name == "array_el":
                    code += "<->"
                    code += str(arg.value['index'])
                code += "\n"
                return code
            else:
                code += str( type(arg) )
                code += "<->"
                code += repr(arg)
                code += "\n"
                return code
                
            

        
        
        
        
        
