symbols<->
kw_integer<->a
kw_integer<->b
kw_integer<->i
kw_string<->test
kw_boolean<->a_bool
kw_array<->nums<->1<->10<->kw_integer
;code:
int_lit<->1
id<->a
func<->:=
message<->done
int_lit<->5
id<->b
func<->:=
message<->done
id<->a
id<->b
int_lit<->2
func<->*
func<->-
func<->(
int_lit<->2
func<->-
id<->a
func<->:=
message<->done
kw_true<->true
id<->a_bool
func<->:=
message<->done
id<->a_bool
func<->not
id<->a_bool
func<->:=
message<->done
string_lit<->'write:'
func<->write
message<->done
id<->a_bool
string_lit<->'a_bool is'
id<->a
string_lit<->'a is'
func<->write
message<->done
string_lit<->'hello world'
id<->test
func<->:=
message<->done
id<->test
id<->test
func<->+
func<->write
message<->done
string_lit<->'\nif statement:'
func<->write
message<->done
<type 'int'><->107
int_lit<->1
int_lit<->2
func<->=
func<->(
func<->not
func<->if
message<->done
string_lit<->'I"m in an if'
func<->write
message<->done
<type 'int'><->107
func<->location<->107
message<->done
string_lit<->'\nwhile loop'
func<->write
message<->done
int_lit<->0
id<->i
func<->:=
message<->done
<type 'int'><->117
func<->location<->117
message<->done
<type 'int'><->138
id<->i
int_lit<->5
int_lit<->2
func<->*
func<-><
func<->while
message<->done
id<->i
int_lit<->1
func<->+
id<->i
func<->:=
message<->done
id<->i
string_lit<->'i is '
func<->write
message<->done
<type 'int'><->117
func<->jmp
message<->done
<type 'int'><->138
func<->location<->138
message<->done
string_lit<->'\nrepeat until:'
func<->write
message<->done
int_lit<->0
id<->i
func<->:=
message<->done
<type 'int'><->149
func<->location<->149
message<->done
id<->i
int_lit<->1
func<->+
id<->i
func<->:=
message<->done
id<->i
string_lit<->'i is '
func<->write
message<->done
<type 'int'><->149
id<->i
int_lit<->5
func<->=
func<->until
message<->done
string_lit<->'\narrays:'
func<->write
message<->done
int_lit<->4
array_el<->nums<->3
func<->:=
message<->done
int_lit<->0
int_lit<->5
func<->-
array_el<->nums<->7
func<->:=
message<->done
array_el<->nums<->3
array_el<->nums<->1
func<->:=
message<->done
array_el<->nums<->3
array_el<->nums<->7
func<->+
array_el<->nums<->10
func<->:=
message<->done
int_lit<->1
id<->i
func<->:=
message<->done
<type 'int'><->217
func<->location<->217
message<->done
<type 'int'><->243
id<->i
int_lit<->10
func<-><=
func<->while
message<->done
array_el<->nums<->i
string_lit<->'] = '
id<->i
string_lit<->'num['
func<->write
message<->done
id<->i
int_lit<->1
func<->+
id<->i
func<->:=
message<->done
<type 'int'><->217
func<->jmp
message<->done
<type 'int'><->243
func<->location<->243
message<->done
id<->i
string_lit<->'I"m still alive and i is'
func<->write
message<->done
;