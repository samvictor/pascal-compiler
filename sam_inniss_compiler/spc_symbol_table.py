# Symbol Table


class ScopeError(Exception):
    comment = ""
    row = ""
    column = ""
    
    def __init__(self, comment = "", row = "", column = ""):
        self.comment = comment
        self.row = row
        self.column = column
        
    def __str__(self):
        ret_str = "Scope Error in Pascal code: "
        if self.row:
            ret_str += "Line - " + self.row
            if self.column:
                ret_str += ", Column - " + self.column
        if self.comment:
            ret_str += ", comment: " + self.comment
        return ret_str

class SymbolTable:
    tables = {'scope': 0, 'symbols': [], 'size':0, 'children':[], 'parent': None}
    # scope is scope level
    current_table = tables
    scope_trace = [0]
    
    def __init__(self):
        pass
    
    def __str__(self):
        ret_str = ""
        table_list = [self.tables]
        
        for tab in table_list:
            if type(tab) == str:
                ret_str += tab
                continue
                
            ret_str += ""
            ret_str += str(tab['symbols']).replace("),", ")\n") + "\n"
            
            table_list += tab['children']
            table_list += ["\n"]
        
        return ret_str
    
    def Add(self, symbol):
        # adds symbol to current table
        
        if self.FindById(symbol.name, silent = True):
            raise ScopeError(symbol.name + " already exists in scope",
                    row = symbol.row, column = symbol.column)
        
        self.current_table['symbols'] += [symbol]
        self.current_table['size'] += 1
    
    def FindById(self, id, silent = False):
        # there should only be one symbol in each table with a given name
        if type(id) == str:
            in_name = id
        else:
            in_name = id.name
            
        current_table = self.current_table
        while True:
            if not current_table:
                if not silent:
                    print "token with name " + in_name + " not found"
                return None
            for s in current_table['symbols']:
                if s.name == in_name:
                    return s
            
            # go backwards through all tables/scopes
            current_table = current_table['parent']
    
    def OpenScope(self):
        self.scope_trace += [ len(self.current_table['children']) ]
        
        current_level = self.current_table['scope']
        current_level += 1
        self.current_table['children'] += [{'scope': current_level, 'symbols': [], 'size':0,
                                            'children': [], 'parent': self.current_table}]
        
        self.current_table = self.current_table['children'][-1]
        
    def CloseScope(self, last_token = None):
        self.scope_trace = self.scope_trace[:-1]
        self.current_table = self.current_table['parent']
        if not self.current_table:
            if last_token:
                raise ScopeError("Closed scope that wasn't opened",
                    row = last_token.row, column = last_token.column)
            else:
                raise ScopeError("Closed scope that wasn't opened")
    
    def CreateStackCode(self):
        code = "symbols<->\n"
        for sym in self.tables['symbols']:
            code += sym.value['type'].type.name
            code += "<->"
            code += sym.name
            
            sym_type = sym.value['type']
            if sym_type.type.name == "kw_array":
                code += "<->"
                code += str(sym_type.value['min'])
                code += "<->"
                code += str(sym_type.value['max'])
                code += "<->"
                code += sym_type.value['type'].type.name
            
            code += "\n"
        
        code += ";"
        return code
        
    