#usage python spc.py hello.pp
#FPC hello
import sys
import string

from spc_tokens import *

class BadSyntax(Exception):
    files = None
    comment = ""
    def __init__(self, files, comment = ""):
        self.files = files
        self.comment = comment
        
    def __str__(self):
        ret_str = "Bad Syntax in Pascal code: "
        ret_str += "Line - " + str(self.files[-1]['row'])
        ret_str += ", Column - " + str(self.files[-1]['column'])
        if self.comment:
            ret_str += ", comment: " + self.comment
        return ret_str
        
class Scanner:
    cchar = None
    code = None
    index = 0
    token_breakers = string.whitespace
    read = ""
    last_token = None
    files = None
    token_list = None
    
    def __init__(self, in_code, in_files, in_tk_list):
        self.code = in_code
        self.files = in_files
        self.token_list = in_tk_list
    
    def NextToken(self):
        self.read = ""
        done = False
        while (not done): # should be at start of token
            cchar = self.NextChar()
            
            if cchar in self.token_breakers:
                # read a white space? keep looking
                done = False
                continue
                
            if cchar == "{":    
                while cchar != "}":                        
                    self.read += cchar
                    cchar = self.NextChar()
                
                self.read += cchar
                #print "dumping comment:"
                #print self.read
                self.read = ""
                done = False
                continue
            
            if cchar == "'":
                self.read = "'"
                cchar = self.NextChar()
                
                while cchar != "'":
                    self.read += cchar
                    cchar = self.NextChar()
                
                self.read += cchar
                
                self.last_token = Token(TokenType.string_lit, value = self.read,
                                name = self.read,
                                row = self.files[-1]['row'], column = self.files[-1]['column'])
                return self.last_token
                
            
            if cchar in string.letters + "_": # identifier or kw
                while cchar in string.letters + string.digits +"_":
                    self.read += cchar                    
                    cchar = self.NextChar()                    
                
                self.index -= 1
                return self.TokenFromRead()
            
            if cchar in string.digits:
                dot_count = 0
                is_int = True
                while cchar in string.digits + ".":
                    if cchar != ".":
                        self.read += cchar
                        cchar = self.NextChar()
                    elif dot_count:
                        raise BadSyntax(self.files, "number has too many dots")
                    else:
                        if self.CheckNextChar() == ".": # might be range (..)
                            break
                            
                        dot_count += 1
                        is_int = False
                        
                        self.read += cchar
                        cchar = self.NextChar()
                        
                
                self.index -= 1
                if is_int:
                    new_num = int (self.read)
                    self.last_token = Token(TokenType.int_lit, name = str(new_num), 
                                value = new_num,
                                row = self.files[-1]['row'], column = self.files[-1]['column'])
                else:
                    new_num = float(self.read)
                    self.last_token = Token(TokenType.real_lit, name = str(new_num), 
                                value = new_num,
                                row = self.files[-1]['row'], column = self.files[-1]['column'])
                
                return self.last_token
            
            if cchar == ".":
                if self.token_list[-1].GetType() == "kw_end":
                    return Token(TokenType.eof, name = ".")
                elif self.CheckNextChar() == ".":
                    if self.CheckNextChar(2) == ".":
                        raise BadSyntax(self.files, "3 consecutive dots not allowed")
                    else:
                        self.index += 1
                        return Token(TokenType.op_2dots, name = "..",
                                row = self.files[-1]['row'], column = self.files[-1]['column'])
                else:
                    return Token(TokenType.op_dot, name = ".",
                                row = self.files[-1]['row'], column = self.files[-1]['column'])
            
            if cchar == ";":
                return Token(TokenType.op_semi, name = ";")

            if cchar == "(":
                return Token(TokenType.op_open_paren, name = "(")

            if cchar == ")":
                return Token(TokenType.op_close_paren, name = ")")
            
            if cchar == "=":
                return Token(TokenType.op_eq, name = "=")
            
            if cchar == "+":
                return Token(TokenType.op_plus, name = "+")
            
            if cchar == "-":
                return Token(TokenType.op_minus, name = "-")
            
            if cchar == "*":
                return Token(TokenType.op_multiply, name = "*")
            
            if cchar == "/":
                return Token(TokenType.op_div, name = "/")
            
            if cchar == ":":
                if self.CheckNextChar() == "=":
                    self.index += 1
                    return Token(TokenType.op_colon_eq, name = ":=")
                else:
                    return Token(TokenType.op_colon, name = ":")
            
            if cchar == "[":
                return Token(TokenType.op_open_bracket, name = "[")
            
            if cchar == "]":
                return Token(TokenType.op_close_bracket, name = "]")
                
            if cchar == ",":
                return Token(TokenType.op_comma, name = "','")
                
            if cchar == "<":
                if self.CheckNextChar() == ">":
                    self.index += 1
                    return Token(TokenType.op_not_eq, name = "<>")
                    
                elif self.CheckNextChar() == "=":
                    self.index += 1
                    return Token(TokenType.op_less_eq, name = "<=")
                else:
                    return Token(TokenType.op_less, name = "<")
                
            if cchar == ">":
                if self.CheckNextChar() == "=":
                    self.index += 1
                    return Token(TokenType.op_greater_eq, name = ">=")
                else:
                    return Token(TokenType.op_greater, name = ">")
            
            if cchar == "^":
                #raise BadSyntax(self.files, "Pointers not supported")
                print "Error: pointers '^' not supported"
                
            if cchar == "&":
                return Token(TokenType.kw_and, name = "&")
                
            if cchar == "|":
                return Token(TokenType.kw_or, name = "|")
                
            if cchar in "!~":
                return Token(TokenType.kw_not, name = "!")
            
            
            
            
            print "default:", cchar
            self.read = cchar
            return self.TokenFromRead()
            #print
        
        return self.TokenFromRead()
    
    def NextChar(self):
        try:
            cchar = self.code[self.index]            
        except IndexError:
            raise EOFError
            
        self.index += 1
        
        if cchar == "\n":
            self.files[-1]['column'] = 1
            self.files[-1]['row'] += 1
        else:
            self.files[-1]['column'] += 1
        
        return cchar
        
    def CheckNextChar(self, distance = 1):
        try:
            cchar = self.code[self.index + distance - 1]            
        except IndexError:
            cchar = None
            
        return cchar
    
    def TokenFromRead(self, read = None):
        if not read:
            read = self.read
        
        if not read:
            return None
        read = read.lower()

        try: # try to make a keyword token out of read
            key_name = "kw_"+read
            self.last_token = Token(TokenType[key_name], name = read,
                                row = self.files[-1]['row'], column = self.files[-1]['column'])
        except:
            self.last_token = Token(TokenType.id, name = read,
                                row = self.files[-1]['row'], column = self.files[-1]['column'])
        
        return self.last_token
    





