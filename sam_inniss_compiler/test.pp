Program
    
{
    comment
}


var 
    a, b, i : integer;

var test : string;
var a_bool: boolean;
var nums: array [1..10] of integer; 

begin
    {math}
    a := 1;
    b := 5;
    a := (a - b * 2)-2;
    
    a_bool := true;
    a_bool := not a_bool;
    
    
    {write}
    write('write:');
    write('a is', a, 'a_bool is', a_bool);
    
    test := 'hello world';
    write(test+test);
    
    {if statement}
    write('\nif statement:');
    if not (1 = 2) then write('I"m in an if');
    
    {while loop}
    write('\nwhile loop');
    i := 0;
    while i < 5*2 do
    begin
        i :=  i + 1;
        write('i is ', i);
    end;
    
    {repeat-until loop}
    write('\nrepeat until:');
    i := 0;
    repeat
        i := i+1;
        write('i is ', i);
        
    until i = 5;    
    
    {arrays}
    write('\narrays:');
    nums[3] := 4;
    nums[7] := -5;
    nums[1] := nums[3];
    
    nums[10] := nums[3] + nums[7];
    
    i := 1;
    
    while i <= 10 do
    begin
        write('num[', i, '] = ', nums[i]);
        i := i + 1;
    end
    
    write ('I"m still alive and i is', i);
    
end.