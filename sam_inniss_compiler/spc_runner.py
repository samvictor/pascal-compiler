# SPC runner 
# usage python spc_runner.py file.spc
import sys

f = open(sys.argv[1], 'r')
print "Running", sys.argv[1]
print

in_code = f.read()

# Symbol Table
tokens = in_code.split(';')[0]
tokens = tokens.split('\n')[1:-1]
tokens = map(lambda x: x.split('<->'), tokens)

for t in tokens:
    if t[0] == "kw_integer":
        t += [{'value': 0}]
    elif t[0] == "kw_real":
        t += [{'value': 0.0}]
    elif t[0] == "kw_string":
        t += [{'value': ""}]
    elif t[0] == "kw_boolean":
        t += [{'value': False}]
    elif t[0] == "kw_char":
        t += [{'value': ""}]
    elif t[0] == "kw_array":
        t_min = int(t[2])
        t_max = int(t[3])
        t_type = t[4]
        
        t.insert(2, {'value': [0]*(abs(t_max) +1+ (t_max - t_min)), 'min': t_min,
                        'max': t_max, 'type': t_type, 
                        'size': t_max - t_min} )

def ArrayGet(array, index):
    try:
        index = int(index)
    except:
        index = GetFromToken(["kw_integer", index])
    
    if array['min'] <= index <= array['max']:
        return array['value'][index - array['min']]
    else:
        print "Error: index out of range"

def ArraySet(array, index, value):
    try:
        index = int(index)
    except:
        index = GetFromToken(["kw_integer", index])
        
    if array['min'] <= index <= array['max']:
        array['value'][index - array['min']] = value
    else:
        print "Error: index out of range"

def FindToken(name):
    for t in tokens:
        if t[1] == name:
            return t
    print "error finding token: " + name + " not defined"

def AssignToToken(id, value):
    name = id[1]
    
    for i, t in enumerate(tokens):
        if t[1] == name:
            if id[0] == "array_el":
                ArraySet(tokens[i][2], id[2], value)
                return 
            tokens[i][2]['value'] = value
            return
    print "error - token assignment: " + name + " not defined"

def GetFromToken(id):
    name = id[1]
    for i, t in enumerate(tokens):
        if t[1] == name:
            temp_value = tokens[i][2]['value']
            if t[0] == "kw_integer":
                temp_value = int(temp_value)
            elif t[0] == "kw_real":
                temp_value = float(temp_value)
            elif t[0] == "kw_boolean":
                if type(temp_value) == str:
                    temp_value = eval(temp_value.capitalize())
            elif t[0] == "kw_char":
                pass
            elif t[0] == "kw_string":
                pass
            elif t[0] == "kw_array":
                temp_value = ArrayGet(t[2], id[2])
            
            return temp_value
    print "error getting token: " + str(name) + " not defined"
            
            
# Operations
code = in_code.split(';')[1]
code = code.split('\n')[1:-1]
code = map(lambda x: x.split('<->'), code)

stack = []

def EvalTypes(source1, source2 = None): 
    if source1[0] == "int_lit":
        source1[1] = int(source1[1])
    elif source1[0] == "real_lit":
        source1[1] = float(source1[1])
    elif source1[0] == "string_lit":
        pass
    elif source1[0] == "boolean_lit":
        if type(source1[1]) == str:
            source1[1] = eval(source1[1].capitalize())
    elif source1[0] == "char_lit":
        pass
    elif source1[0] == "id" or source1[0] == "array_el":
        source1[1] = GetFromToken(source1)
    elif source1[0] == "kw_true":
        source1[1] = True
    elif source1[0] == "kw_false":
        source1[1] = False
    
    if not source2:
        return source1[1]
    
    if source2[0] == "int_lit":
        source2[1] = int(source2[1])
    elif source2[0] == "real_lit":
        source2[1] = float(source2[1])
    elif source2[0] == "string_lit":
        pass
    elif source2[0] == "boolean_lit":
        if type(source2[1]) == str:
            source2[1] = eval(source2[1].capitalize())
    elif source2[0] == "char_lit":
        pass
    elif source2[0] == "id" or source2[0] == "array_el":
        source2[1] = GetFromToken(source2)
    elif source2[0] == "kw_true":
        source2[1] = True
    elif source2[0] == "kw_false":
        source2[1] = False
    
    return source1[1], source2[1]


def FindLocationFromId(in_code, id):
    for i, c in enumerate(code):
        if c[1] == "location":
            if c[2] == id:
                return i

i = -1
while i < len(code) - 1:
    i += 1
    e = code[i]
    if e[0] == "func":
        if e[1] == ":=":
            target = stack.pop()
            source = stack.pop()
            target_token = FindToken(target[1])
            
            source = EvalTypes(source)
            
            AssignToToken(target, source)
        
        elif e[1] == "writeln" or e[1] == "write":
            print_str = ""
            while True:
                if stack[-1][0] == "message" and stack[-1][1] == "done":
                    break
                
                to_print = stack.pop()
                if to_print[0] == 'id' or to_print[0] == 'array_el':
                    to_print = GetFromToken(to_print)
                
                if type (to_print) == list:
                    to_print = to_print[1]
                
                if type(to_print) is str:
                    print_str += eval(to_print)
                else:
                    print_str += str(to_print)
                
                print_str += " "
                
            print print_str
        
        elif e[1] == "+":
            source2 = stack.pop()
            source1 = stack.pop()
            s_type = source1[0]
            
            source1, source2 = EvalTypes(source1, source2)
            
            result = source1 + source2
            
            if s_type == "id" or s_type == "array_el":
                try:
                    float(result)
                    s_type = "real_lit"
                except:
                    s_type = "string_lit"
                
            stack.append([s_type, result])
            
        elif e[1] == "-":
            source2 = stack.pop()
            source1 = stack.pop()
            s_type = source1[0]
            
            source1, source2 = EvalTypes(source1, source2)
            result = source1 - source2
            
            if s_type == "id" or s_type == "array_el":
                try:
                    float(result)
                    s_type = "real_lit"
                except:
                    s_type = "string_lit"
                
            stack.append([s_type, result])
            
        elif e[1] == "/":
            source2 = stack.pop()
            source1 = stack.pop()
            s_type = source1[0]
            
            source1, source2 = EvalTypes(source1, source2)
            result = source1 / source2
            
            if s_type == "id" or s_type == "array_el":
                try:
                    float(result)
                    s_type = "real_lit"
                except:
                    s_type = "string_lit"
                
            stack.append([s_type, result])
            
        elif e[1] == "*":
            source2 = stack.pop()
            source1 = stack.pop()
            s_type = source1[0]
            
            source1, source2 = EvalTypes(source1, source2)
            result = source1 * source2
            
            if s_type == "id" or s_type == "array_el":
                try:
                    float(result)
                    s_type = "real_lit"
                except:
                    s_type = "string_lit"
                
            stack.append([s_type, result])
            
        elif e[1] == "&" or e[1] == "and":
            source2 = stack.pop()
            source1 = stack.pop()
            s_type = source1[0]
            
            source1, source2 = EvalTypes(source1, source2)
            result = source1 and source2
            
            if s_type == "id" or s_type == "array_el":
                s_type = "boolean_lit"
                
            stack.append([s_type, result])
            
        elif e[1] == "|" or e[1] == "or":
            source2 = stack.pop()
            source1 = stack.pop()
            s_type = source1[0]
            
            source1, source2 = EvalTypes(source1, source2)
            result = source1 or source2
            
            if s_type == "id" or s_type == "array_el":
                s_type = "boolean_lit"
                
            stack.append([s_type, result])
            
        elif e[1] == "=":
            source2 = stack.pop()
            source1 = stack.pop()
            s_type = source1[0]
            
            source1, source2 = EvalTypes(source1, source2)
            result = source1 == source2
            
            if s_type == "id" or s_type == "array_el":
                s_type = "boolean_lit"
                
            stack.append([s_type, result])
            
        elif e[1] == "<>":
            source2 = stack.pop()
            source1 = stack.pop()
            s_type = source1[0]
            
            source1, source2 = EvalTypes(source1, source2)
            result = source1 != source2
            
            if s_type == "id" or s_type == "array_el":
                s_type = "boolean_lit"
                
            stack.append([s_type, result])
            
        elif e[1] == "<":
            source2 = stack.pop()
            source1 = stack.pop()
            s_type = source1[0]
            
            source1, source2 = EvalTypes(source1, source2)
            result = source1 < source2
            
            if s_type == "id" or s_type == "array_el":
                s_type = "boolean_lit"
                
            stack.append([s_type, result])
            
        elif e[1] == ">":
            source2 = stack.pop()
            source1 = stack.pop()
            s_type = source1[0]
            
            source1, source2 = EvalTypes(source1, source2)
            result = source1 > source2
            
            if s_type == "id" or s_type == "array_el":
                s_type = "boolean_lit"
                
            stack.append([s_type, result])
            
        elif e[1] == "<=":
            source2 = stack.pop()
            source1 = stack.pop()
            s_type = source1[0]
            
            source1, source2 = EvalTypes(source1, source2)
            result = source1 <= source2
            
            if s_type == "id" or s_type == "array_el":
                s_type = "boolean_lit"
                
            stack.append([s_type, result])
            
        elif e[1] == ">=":
            source2 = stack.pop()
            source1 = stack.pop()
            s_type = source1[0]
            
            source1, source2 = EvalTypes(source1, source2)
            result = source1 >= source2
            
            if s_type == "id" or s_type == "array_el":
                s_type = "boolean_lit"
                
            stack.append([s_type, result])
            
        elif e[1] == "=":
            source2 = stack.pop()
            source1 = stack.pop()
            s_type = source1[0]
            
            source1, source2 = EvalTypes(source1, source2)
            result = source1 == source2
            
            if s_type == "id" or s_type == "array_el":
                s_type = "boolean_lit"
                
            stack.append([s_type, result])
            
        elif e[1] == "not":
            source1 = stack.pop()
            s_type = source1[0]
            
            source1 = EvalTypes(source1)
            result = source1
            
            if result:
                stack.append(['kw_false', 'false'])
            else:
                stack.append(['kw_true', 'true'])
            
        elif e[1] == "(":
            source1 = stack.pop()
            s_type = source1[0]
            source1 = EvalTypes(source1)
            result = source1
            
            if s_type == "id" or s_type == "array_el":
                try:
                    float(result)
                    s_type = "real_lit"
                except:
                    s_type = "string_lit"
                
            stack.append([s_type, result])
        
        elif e[1] == "if":
            cond = stack.pop()
            s_type = cond[0]
            cond = EvalTypes(cond)
            
            dest = stack.pop()[1]
            
            if not cond:
                i = FindLocationFromId(code, dest)
                continue
        
        elif e[1] == "while":
            cond = stack.pop()
            s_type = cond[0]
            cond = EvalTypes(cond)
            
            dest = stack.pop()[1]
            
            if not cond:
                i = FindLocationFromId(code, dest)
                continue
        
        elif e[1] == "until":
            cond = stack.pop()
            s_type = cond[0]
            cond = EvalTypes(cond)
            
            dest = stack.pop()[1]
            
            if not cond:
                i = FindLocationFromId(code, dest)
                continue
        
        elif e[1] == "jmp":
            loc = stack.pop()[1]
            i = FindLocationFromId(code, loc)
            
    else:
        stack.append(e[:])
        
                
            
            

#print "tokens are ", tokens
#print "\ncode is ", code
#print "\nstack is ", stack