import sys
import string
from enum import Enum, unique

@unique
class TokenType(Enum):
    id = 1 # identifier
    eof = 2
    real_lit = 3
    int_lit = 4
    string_lit = 5
    func = 84
    boolean_lit = 86
    char_lit = 87
    
    jmp = 89
    location = 88
    array_el = 91
    
    # keywords
    kw_and = 7
    kw_array = 8
    kw_asm = 9
    kw_begin = 10
    kw_break = 11
    kw_case = 12
    kw_const = 13
    kw_constructor = 14
    kw_continue = 15
    kw_destructor = 16
    kw_div = 17
    kw_do = 18
    kw_downto = 19
    kw_down = 90
    kw_else = 20
    kw_end = 21
    kw_false = 22
    kw_file = 23
    kw_for = 24
    kw_function = 25
    kw_goto = 26
    kw_if = 27
    kw_implementation = 28
    kw_in = 29
    kw_inline = 30
    kw_interface = 31
    kw_label = 32
    kw_mod = 33
    kw_nil = 34
    kw_not = 35
    kw_object = 36
    kw_of = 37
    kw_on = 38
    kw_operator = 39
    kw_or = 40
    kw_packed = 41
    kw_procedure = 42
    kw_program = 43
    kw_record = 44
    kw_repeat = 45
    kw_set = 46
    kw_shl = 47
    kw_shr = 48
    kw_then = 50
    kw_to = 51
    kw_true = 52
    kw_type = 53
    kw_unit = 54
    kw_until = 55
    kw_uses = 56
    kw_var = 57
    kw_while = 58
    kw_with = 59
    kw_xor = 60
    
    kw_string = 49
    kw_integer = 81
    kw_real = 82
    kw_boolean = 83
    kw_char = 85
    
    # operators
    op_semi = 61
    op_open_paren = 62
    op_close_paren = 63
    
    op_eq = 64
    op_plus = 65
    op_minus = 66
    op_multiply = 67
    op_div = 68
    
    op_colon = 69
    op_open_bracket = 70
    op_close_bracket = 71
    op_comma = 72
    op_2dots = 73
    op_dot = 80
    
    
    op_less = 74
    op_greater = 75
    op_less_eq = 76
    op_greater_eq = 77
    op_not_eq = 78
    op_colon_eq = 79
    
    92
    
token_count = 1
class Token:
    type = None  # token type
    value = None # depends on token type
    name = None  # string
    row = "-1"
    column = "-1"
    id = 0
    
    def __init__(self, type, value="", name="", row = "-1", column = "-1"):
        self.type = type
        if value:
            self.value = value
        else:
            self.value = {}
            
        self.name = name
        self.row = str(row)
        self.column = str(column)
        
        global token_count
        self.id = token_count
        token_count += 1
    
    def GetType(self):
        return self.type.name
    
    def __str__(self):
        return self.name
        ret_str =  "type: " + self.type.name
        ret_str += "\nvalue: " + str(self.value)
        ret_str += "\nname: " + self.name
        ret_str += "\nrow: " + self.row
        return ret_str
    
    def __repr__(self):
        return self.name
        ret_str =  "(type: " + self.type.name
        ret_str += ", value: " + str(self.value)
        ret_str += ", name: " + self.name
        ret_str += ", row: " + self.row +")"
        return ret_str
    
    def MakeCopy(self, new_id = False):
        new_token = Token(self.type, self.value, self.name, self.row, self.column)
        new_token.id = self.id
        
        global token_count
        if new_id:
            new_token.id = token_count
            token_count += 1
            
        return new_token
